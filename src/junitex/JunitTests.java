package junitex;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofMinutes;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.CountDownLatch;



import org.junit.jupiter.api.Test;
public class JunitTests {
    @Test
   public void  testarrayEqual(){
       String[] expected = {"Honda", "Kawasaki", "BMW"};

       String[] test1 = {"Honda", "Kawasaki", "BMW"};
       assertArrayEquals(expected, test1);

       }
    @Test
    public void testequal(){

       int x = 3 * 3 - 4;
       assertEquals(5,x);
    }
    @Test
    public void testtrue(){
        boolean x = true;
        assertTrue(x);
    }
    @Test
    public void testfalse(){
        boolean x = true; ///intentionally left "true"
        assertFalse(x);

    }
    @Test
    public void testtnotnull(){
    String x = null; //will fail
    assertNotNull(x);
    }
    @Test
    public void testtnotsame(){
        int x = 5423452;
        assertNotSame(5435234, x);

    }
    @Test
    public void testtnull(){
        String x = null;
        assertNull(x);

    }
    @Test
    public void testsame(){
        String x = "12341234";
        assertSame("12341234",x);

    }
    @Test
    public void testthat(){
        int x = 3;
        assertThat(x, is(3));
        assertThat(x, is(not(4)));

    }

}
